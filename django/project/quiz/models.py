from django.db import models

class Question(models.Model):
    text = models.CharField(max_length=100)     # The Content of the Question, e.g. 'Which option best describes the sun?'

    def __str__(self):
        return self.text

class Quiz(models.Model):
    title = models.CharField(max_length=100)    # The Title of the Quiz, e.g. '2020 Final Exam'
    questions = models.ManyToManyField(Question, blank=True) # Link to Questions

    def __str__(self):
        return self.title

class Option(models.Model):
    text = models.CharField(max_length=200)     # The option of the Question, e.g. 'Hot'
    mark = models.DecimalField(max_digits=5, decimal_places=2)  # The mark of the option, e.g. '2.0'
    question = models.ForeignKey(Question, on_delete=models.CASCADE) # Link to Questions
    
    def __str__(self):
        return self.text