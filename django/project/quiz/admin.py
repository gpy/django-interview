from django.contrib import admin
from .models import Quiz, Question, Option

class OptionInline(admin.StackedInline):
    model = Option
    extra = 0

class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [("Question", {'fields': ['text']}),]
    inlines = [OptionInline]

admin.site.register(Question, QuestionAdmin)
admin.site.register(Quiz)