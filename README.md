# Environment
Python 3.9.2 (64-bit),
Django 3.1.7
Python 3 venv (Virtual Environment)

To ensure that this program can run on different computers, it has been configured and isolated using a virtual environment.

# Activate the Virtual Environment
Windows
```
.\django\environment\Scripts\activate
```

# Run Django Server
```
.\django\project\python manage.py runserver
```

# About the Project
1. Go to http://ip:port/ to automatically generate data firstly.
2. Then go to http://ip:port/admin to manage the database.

### SuperUser Information
User Name: admin
Password: 123
